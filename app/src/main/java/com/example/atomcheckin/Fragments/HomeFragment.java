package com.example.atomcheckin.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.atomcheckin.R;
public class HomeFragment extends Fragment {

    View view;
    TextView textView;

    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        textView = view.findViewById(R.id.MyHome);
        textView.setText("Home Fragment");
        return view;
    }

}
