package com.example.atomcheckin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {

    private static final String TAG = "SignUp";

    EditText txt_fullname;
    EditText txt_email;
    EditText txt_password;
    EditText txt_confirmpassword;
    Button btn_register;

    FirebaseAuth firebaseAuth;
    FirebaseFirestore db;
    ProgressBar progressBar;

    String userID;
    String fullname;
    String email;
    String password;
    String confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        //html css
        txt_fullname = findViewById(R.id.txt_fullname);
        txt_email = findViewById(R.id.txt_email);
        txt_password = findViewById(R.id.txt_password);
        txt_confirmpassword = findViewById(R.id.txt_confirmpassword);
        btn_register = findViewById(R.id.btn_register);

        firebaseAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        progressBar = findViewById(R.id.progressBar);

        if (firebaseAuth.getCurrentUser() != null) {
            startActivity(new Intent(SignUp.this, MainActivity.class));
            finish();
        }

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fullname = txt_fullname.getText().toString();
                email = txt_email.getText().toString();
                password = txt_password.getText().toString();
                confirmPassword = txt_confirmpassword.getText().toString();

                if (TextUtils.isEmpty(fullname)) {
                    txt_fullname.setError("Full name is required");
                    return;
                }

                if (TextUtils.isEmpty(email)) {
                    txt_email.setError("Email is required");
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    txt_password.setError("Password is required");
                    return;
                }

                if (TextUtils.isEmpty(confirmPassword)) {
                    txt_confirmpassword.setError("Confirm Password is required");
                    return;
                }

                if (!(password.equals(confirmPassword))) {
                    Toast.makeText(SignUp.this, "Password is not math", Toast.LENGTH_LONG).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(SignUp.this, "User Created", Toast.LENGTH_LONG).show();

                            userID = firebaseAuth.getCurrentUser().getUid();
                            DocumentReference documentReference = db.collection("users").document(userID);

                            Map<String, Object> user = new HashMap<>();
                            user.put("fullname", fullname);
                            user.put("email", email);
                            user.put("password", password);

                            documentReference.set(user)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "Success : user profile is created for " + userID);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Failure : " + e.toString());
                                        }
                                    });

                            startActivity(new Intent(SignUp.this, MainActivity.class));
                        } else {
                            Toast.makeText(SignUp.this, "Error : " + task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });
    }

    public void newSignIN(View view) {
        Intent i = new Intent(SignUp.this, SignIN.class);
        startActivity(i);
    }
}
